FROM ubuntu

RUN apt-get update && apt-get install -y \
    python3-pip python-dev

COPY ./requirements.txt /app/requirement.txt

WORKDIR /app

RUN pip3 install -r requirement.txt

COPY .  ./app

ENTRYPOINT ["python"]

CMD ["app.py"]